##############################################
 #  @filename   :   main.py                 #
 #  @brief      :   2.7inch e-paper display #
 #  @author     :   Kevin Stillman          #
##############################################



import time
import epd2in7
import requests
import json
import RPi.GPIO as GPIO
import python_weather
import asyncio
import os
from bs4 import BeautifulSoup
from PIL import Image 
from PIL import ImageFont
from PIL import ImageDraw

# SCREENS:
# 0 = WORKOUTS
# 1 = WEATHER

screens = ['workouts', 'weather']
currentscreen = 0

def screenchanged(screen):
    if screen == 0 or screen == 'workouts':
        currentscreen = 0
    elif screen == 1 or screen == 'weather':
        currentscreen = 1
        
bc = 1

def buttoncycle():
    global bc
    buttoncount = bc
    print(f'old bc: {buttoncount}')
    buttoncount += 1
    print(f'new bc: {buttoncount}')
    if buttoncount > len(screens):
        print(f'{buttoncount} > {len(screens)}, bad!')
        buttoncount = 1
        bc = buttoncount
        print(f'bc = {bc} | This should be 1')
        return buttoncount
    else:
        bc = buttoncount
        return buttoncount
    
def button_press(channel):
    if channel == button1_pin:
        print(f'Button 1 pressed! Cycling...')
        buildScreen(buttoncycle())

    if channel == button2_pin:
        print('no use yet!')
    
    if channel == button3_pin:
        print('Button 3 pressed! Clearing screen..')
        epd = epd2in7.EPD()
        epd.init()
        image = Image.new('1', (epd2in7.EPD_WIDTH, epd2in7.EPD_HEIGHT), 255)    # 255
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf', 18)
        
        epd.display_frame(epd.get_frame_buffer(image)) # 125 to 129 clear hte screen
        print('button 3 press draw executed')
    
    if channel == button4_pin:
        print('Button 4 pressed! This is the quit button, goodbye!')
        quit()
        
def buildScreen(screen):
    if screen == 'workouts' or screen == 1:
            #init building to screen
        epd = epd2in7.EPD()
        epd.init()
        image = Image.new('1', (epd2in7.EPD_WIDTH, epd2in7.EPD_HEIGHT), 255)    # 255: clear the image with white
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf', 18)
        
        # Grab data
        with open('values.json') as f:
            data = json.load(f)
            
        workout_data = {}
        
        for person in data['data']:
            key = '{}_{}'.format(person['name'].lower(), 'data')
            
            workout_data[key] = person
        
        p1hang = str(workout_data['kevin_data']['hang'])
        p1pull = str(workout_data['kevin_data']['pull'])
        p1push = str(workout_data['kevin_data']['push'])
        p1dip = str(workout_data['kevin_data']['dip'])
        p1sit = str(workout_data['kevin_data']['sit'])
        p1plank = str(workout_data['kevin_data']['plank'])
        p1squat = str(workout_data['kevin_data']['squat'])
        p1weight = str(workout_data['kevin_data']['weight'])
        
        p2hang = str(workout_data['justin_data']['hang'])
        p2pull = str(workout_data['justin_data']['pull'])
        p2push = str(workout_data['justin_data']['push'])
        p2dip = str(workout_data['justin_data']['dip'])
        p2sit = str(workout_data['justin_data']['sit'])
        p2plank = str(workout_data['justin_data']['plank'])
        p2squat = str(workout_data['justin_data']['squat'])
        p2weight = str(workout_data['justin_data']['weight'])

        
        
        # FRAME
        draw.text((8, 0), 'Kevin', font = font, fill = 0) #Text 1
        draw.text((105, 0), 'Justin', font = font, fill = 0) #Text 2
        #draw.line((90, 15, 90, 290), fill = 0) #vertical bar under text
        #draw.line((89, 15, 89, 290), fill = 0) #vertical bar under text
        draw.line((0, 15, 180, 15), fill = 0)#horizontal bar under text
        draw.line((0, 16, 180, 16), fill = 0)#horizontal bar under text
        
        
        # Workout labels
        draw.text((65, 40), 'Hang', font = font, fill = 0) # HANG
        draw.text((65, 65), 'Pull', font = font, fill = 0) # Pull ups
        draw.text((65, 90), 'Push', font = font, fill = 0) # Push ups
        draw.text((65, 115), 'Dips', font = font, fill = 0) # Dips
        draw.text((70, 140), 'Sit', font = font, fill = 0) # Sit ups
        draw.text((65, 165), 'Plnk', font = font, fill = 0) # Plank
        draw.text((65, 190), 'Squat', font = font, fill = 0) # Squats
        draw.text((60, 220), 'Weight', font = font, fill = 0) # weigh in
        
        # # DATA LOCATIONS
        
        # FIRST PERSON
        draw.text((3, 40), p1hang, font = font, fill = 0) # HANG
        draw.text((3, 65), p1pull, font = font, fill = 0) # Pull ups
        draw.text((3, 90), p1push, font = font, fill = 0) # Push ups    
        draw.text((3, 115), p1dip, font = font, fill = 0) # Dips
        draw.text((3, 140), p1sit, font = font, fill = 0) # Sit ups
        draw.text((3, 165), p1plank, font = font, fill = 0) # Plank
        draw.text((3, 190), p1squat, font = font, fill = 0) # Squats
        draw.text((3, 220), p1weight, font = font, fill = 0) # weigh in
        
        
        
        # SECOND PERSON
        draw.text((130, 40), p2hang, font = font, fill = 0) # HANG
        draw.text((130, 65), p2pull, font = font, fill = 0) # Pull ups
        draw.text((130, 90), p2push, font = font, fill = 0) # Push ups
        draw.text((130, 115), p2dip, font = font, fill = 0) # Dips
        draw.text((130, 140), p2sit, font = font, fill = 0) # Sit ups
        draw.text((130, 165), p2plank, font = font, fill = 0) # Plank
        draw.text((130, 190), p2squat, font = font, fill = 0) # Squats
        draw.text((130, 220), p2weight, font = font, fill = 0) # weigh in
        

        epd.display_frame(epd.get_frame_buffer(image))
        print('line 151 draw executed')
        
    if screen == 'weather' or screen == 2:
            #init building to screen and clearing
        epd = epd2in7.EPD()
        epd.init()
        image = Image.new('1', (epd2in7.EPD_WIDTH, epd2in7.EPD_HEIGHT), 255)    # 255
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype('/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf', 18)
        
#       epd.display_frame(epd.get_frame_buffer(image)) # 125 to 129 clear hte screen
        
        #prepare weather data and weather screen frame
    
        draw.text((35, 5), 'Norfolk, Va', font = font, fill = 0)
        draw.text((105, 40), 'F', font = font, fill = 0)
        draw.text((10, 60), 'Chichester PA', font = font, fill = 0)
        draw.text((105, 80), 'F', font = font, fill = 0)
        
        epd.display_frame(epd.get_frame_buffer(image)) # draw non-live data
        print('line 162 draw executed')
        time.sleep(3)
        
        # data
        try:
            nfktemp = str(asyncio.run(getweather('Norfolk VA')))
            try:
                chitemp = str(asyncio.run(getweather('Chichester PA')))
            except:
                weatherbroken = True
                print('aw bud the chi temp is fucked')
                
        except:
            weatherbroken = True
            print('aw bud the nfk temp is fucked')
        
        
        
        else:
            draw.text((80, 80), chitemp, font = font, fill = 0, size = 40)
            draw.text((80, 40), nfktemp, font = font, fill = 0, size = 40)
            epd.display_frame(epd.get_frame_buffer(image))
            print('line 181 draw executed')
            
        if weatherbroken:
            image = Image.new('1', (epd2in7.EPD_WIDTH, epd2in7.EPD_HEIGHT), 255)    # clear screen
            draw = ImageDraw.Draw(image)                                            # clear screen
            epd.display_frame(epd.get_frame_buffer(image))                          # clear screen
            draw.text((5, 5), 'Weather broken', font = font, fill = 0)
            
            epd.display_frame(epd.get_frame_buffer(image))
        
# WEATHER

async def getweather(loc):
    async with python_weather.Client(format=python_weather.IMPERIAL) as client:
        weather = await client.get(loc)
        temp = weather.current.temperature
        return temp
        

# BUTTONS
GPIO.setmode(GPIO.BCM)

button1_pin = 5
button2_pin = 6
button3_pin = 13
button4_pin = 19

# set the input pins as pull-downs to avoiud false inputs

GPIO.setup(button1_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(button2_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(button3_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(button4_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.add_event_detect(button1_pin, GPIO.FALLING, callback = button_press, bouncetime = 200)
GPIO.add_event_detect(button2_pin, GPIO.FALLING, callback = button_press, bouncetime = 200)
GPIO.add_event_detect(button3_pin, GPIO.FALLING, callback = button_press, bouncetime = 200)
GPIO.add_event_detect(button4_pin, GPIO.FALLING, callback = button_press, bouncetime = 200)





buildScreen(1)


run = True

while run:
    pass
    
    
    
